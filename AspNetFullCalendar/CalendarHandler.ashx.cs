﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AspNetFullCalendar.Models;
using AspNetFullCalendar.lib;
using Newtonsoft.Json;

namespace AspNetFullCalendar
{
    /// <summary>
    /// Summary description for CalendarHandler
    /// </summary>
    public class CalendarHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {

            var settings = new JsonSerializerSettings ();
            settings.Converters.Add(new DateTimeConverter("yyyy-MM-ddTHH:mm:ss"));

            var today = DateTime.Today;
            var year = today.Year;
            var month = today.Month;
            var day = today.Day;
            var events = new List<CalendarEvent>()
                {
                    new CalendarEvent()
                    {
                    id =1,
                    start = new DateTime(year,month,day,8,0,0),

                    end = new DateTime(year,month,day,9,0,0),
                    title = "preventive"
                    }
                };

            
            var jsonString =  JsonConvert.SerializeObject(events, Formatting.Indented, settings);

            context.Response.ContentType = "application/json";
            context.Response.Write(jsonString);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}