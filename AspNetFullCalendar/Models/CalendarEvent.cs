﻿using System;

namespace AspNetFullCalendar.Models
{
    public class CalendarEvent
    {
        public int id { get; set; }
        public string title { get; set; }
        public DateTime start { get; set; }
        public DateTime? end { get; set; }
        public bool allDay { get; set; }
    }
}