﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="calendar.aspx.cs" Inherits="AspNetFullCalendar.calendar" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        #leftPanel
        {
            float: left;
            width: 15%;
        }
        
        #rightPanel
        {
            float: left;
            width: 80%;
            border: 1px solid #FF0000;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div id="leftPanel">
            <p>
                title <asp:TextBox runat="server"></asp:TextBox>
            </p>
            <p>
                start date <asp:TextBox runat="server"></asp:TextBox>
            </p>
            <p>
                <asp:Button runat="server" Text="Button" />
            </p>
        </div>
        <div id="rightPanel">
            calendar
        </div>
    </div>
    </form>
</body>
</html>
